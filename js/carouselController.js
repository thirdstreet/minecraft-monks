/**
 * Created by damonfloyd on 8/26/15.
 */
/* global angular */
(function() {

    function CarouselController() {
        var _this = this;
        _this.images = [
            { img: 'images/carousel/01.png', caption: "" },
            { img: 'images/carousel/02.png', caption: "" },
            { img: 'images/carousel/03.png', caption: "" },
            { img: 'images/carousel/04.png', caption: "" },
            { img: 'images/carousel/05.png', caption: "" },
            { img: 'images/carousel/06.png', caption: "" },
            { img: 'images/carousel/07.png', caption: "" },
            { img: 'images/carousel/08.png', caption: "" },
            { img: 'images/carousel/09.png', caption: "" },
            { img: 'images/carousel/10.png', caption: "" },
            { img: 'images/carousel/11.png', caption: "" },
            { img: 'images/carousel/12.png', caption: "" },
            { img: 'images/carousel/13.png', caption: "" },
            { img: 'images/carousel/14.png', caption: "" },
            { img: 'images/carousel/15.png', caption: "" },
            { img: 'images/carousel/16.png', caption: "" },
            { img: 'images/carousel/17.png', caption: "" },
            { img: 'images/carousel/18.png', caption: "" },
            { img: 'images/carousel/19.png', caption: "" },
            { img: 'images/carousel/20.png', caption: "" }
        ];

        for(var i = 0; i < _this.images.length; i++) {
            var url = _this.images[i].img;
            url = url + '?text=' + _this.images[i].caption;
            _this.images[i].url = url;
        }
    }

    angular.module('mcmonks.controllers')
        .controller('CarouselController', [CarouselController]);

})();