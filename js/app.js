/**
 * Created by damonfloyd on 8/26/15.
 */
/* global angular */
(function() {

    angular.module('mcmonks.controllers', []);
    angular.module('mcmonks.services', []);
    angular.module('mcmonks.directives', []);

    angular.module(
        'mcmonks',
        [
            'ui.router',
            'mcmonks.controllers',
            'mcmonks.services',
            'mcmonks.directives'
        ]
    ).config(['$urlRouterProvider', function($urlRouterProvider) {
            $urlRouterProvider.otherwise("/");
        }]
    );

})();