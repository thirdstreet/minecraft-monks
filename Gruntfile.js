/**
 * Created by monk on 9/4/15.
 */
/* global module, require, console */

module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        mcmonks: {
            app: 'www',
            build: 'build',
            staging: 'staging'
        },
        clean: [
            '<%= mcmonks.staging %>',
            '<%= mcmonks.build %>',
            '<%= mcmonks.build %>/js',
            '<%= mcmonks.build %>/js/lib',
            '<%= mcmonks.build %>/css',
            '<%= mcmonks.build %>/scripts',
            '<%= mcmonks.build %>/views',
            '<%= mcmonks.build %>/images',
            '<%= mcmonks.build %>/images/monks',
            '<%- mcmonks.build %>/images/carousel'
        ],
        mkdir: {
            target: {
                options: {
                    create: [
                        '<%= mcmonks.staging %>',
                        '<%= mcmonks.build %>',
                        '<%= mcmonks.build %>/js',
                        '<%= mcmonks.build %>/js/lib',
                        '<%= mcmonks.build %>/css',
                        '<%= mcmonks.build %>/scripts',
                        '<%= mcmonks.build %>/views',
                        '<%= mcmonks.build %>/images',
                        '<%= mcmonks.build %>/images/monks',
                        '<%- mcmonks.build %>/images/carousel'
                    ]
                }
            }
        },
        copy: {
            release: {
                files: [
                    {
                        expand: true,
                        cwd: './',
                        src: [
                            'css/*.css',
                            'images/**/*.{png,gif,jpg.svg}',
                            'js/lib/*.js',
                            '*.html',
                            'views/**/*.html'
                        ],
                        dest: 'build'
                    }
                ]
            }
        },
        filerev: {
            options: {
                encoging: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [
                    {
                        src: [
                            'build/scripts/*.js',
                            'build/css/*.css'
                        ]
                    }
                ]
            }
        },
        usemin: {
            html: ['build/*.html'],
            css: ['build/css/*.css'],
            options: {
                assetsDirs: ['build', 'build/css']
            }
        },
        useminPrepare: {
            html: 'index.html',
            options: {
                dest: 'build',
                staging: 'staging'
            }
        }
    });

    grunt.registerTask('default', function() {
        grunt.task.run([
            'clean',
            'mkdir',
            'concat',
            'uglify'
        ]);
    });

    grunt.registerTask('build', 'build the thing.', function() {
        console.log("Here we go!");
        grunt.task.run([
            'clean',
            'mkdir',
            'sass'
        ]);
    });

    grunt.registerTask('test', 'usemin testing...', function() {
        grunt.task.run([
            'clean',
            'mkdir',
            'useminPrepare',
            'copy:release',
            'concat:generated',
            //'cssmin:generated',
            'uglify:generated',
            'filerev',
            'usemin'
        ]);
    });
};