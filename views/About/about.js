/**
 * Created by damonfloyd on 8/26/15.
 */
(function() {

    function AboutController() {
        var _this = this;

    }

    angular.module('mcmonks.controllers')
        .config(['$stateProvider',function($stateProvider) {
            $stateProvider
                .state('About', {
                    url: '/about',
                    templateUrl: 'views/About/about.html'
                });
        }])
        .controller('AboutController', [AboutController]);

})();