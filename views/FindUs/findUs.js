/**
 * Created by monk on 9/3/15.
 */

/* global angular */
(function() {

    function FindUsController() {
        //var _this = this;
    }

    angular.module('mcmonks')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('FindUs', {
                    url: '/findus',
                    templateUrl: 'views/FindUs/findUs.html'
                });
        }])
        .controller('FindUsController', [FindUsController]);

})();