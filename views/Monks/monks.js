/**
 * Created by damonfloyd on 8/26/15.
 */

/*global angular */

(function() {
    function MonksController() {

        var _this = this;
        _this.monks = [
            {
                rank: 'Abbot',
                name: 'Monkrocker',
                aka: 'Monk, D, HaB',
                bio: "Birth.  School.  Video games. Music. Work.  Video games.  Present day.",
                quote: "oh hey...I uh...like your nose ring...",
                image: '../images/monks/Monkrocker.png',
                skin: '',
                class: 'Abbot'
            },
            {
                rank: 'Elder Brother',
                name: 'maxtoo',
                aka: 'Max',
                bio: "",
                quote: "",
                image: '../images/monks/maxtoo.png',
                skin: '',
                class: 'Elder'
            },
            {
                rank: 'Brother',
                name: 'Sproutfoot',
                aka: 'Sprout',
                bio: "",
                quote: "",
                image: '../images/monks/Sproutfoot.png',
                skin: '',
                class: 'Brother'
            },
            {
                rank: 'Brother',
                name: 'VUCat89',
                aka: 'Cat, Monkey\'s Dad',
                bio: "",
                quote: "",
                image: '../images/monks/VUCat89.png',
                skin: '',
                class: 'Brother'
            },
            {
                rank: 'Brother',
                name: 'Monkey832',
                aka: 'Monkey',
                bio: "",
                quote: "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
                image: '../images/monks/default.png',
                skin: '',
                class: 'Brother'
            },
            {
                rank: 'Brother',
                name: 'powertoiletduck',
                aka: 'Duck',
                bio: "",
                quote: "",
                image: '../images/monks/default.png',
                skin: '',
                class: 'Brother'
            }
        ];

        /* template

         {
         rank: 'Brother',
         name: '',
         aka: '',
         bio: "",
         quote: "",
         image: '../images/monks/default.png',
         skin: '',
         class: ''   // same as rank unless they are an Elder Brother, in which case simply Elder
         }

         */


    }

    angular.module('mcmonks')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('Monks', {
                    url: '/monks',
                    templateUrl: 'views/Monks/monks.html'
                });
        }])
        .controller('MonksController', [MonksController]);
})();