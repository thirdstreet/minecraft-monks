/**
 * Created by monk on 8/28/15.
 */
(function() {

    function InfoController() {
        var _this = this;
    }

    angular.module('mcmonks')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('Info', {
                    url: '/info',
                    templateUrl: 'views/Info/info.html'
                }
            );
        }])
        .controller('InfoController', [InfoController]);

})();