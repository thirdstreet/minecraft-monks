/**
 * Created by damonfloyd on 8/26/15.
 */
(function() {
    angular.module('mcmonks')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('Home', {
                    url: '/',
                    templateUrl: 'views/Home/home.html'
                });
        }]);
})();